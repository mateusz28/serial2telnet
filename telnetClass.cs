﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
namespace SerialTelnet
{
public class telnetClass
{
    public AsyncCallback pfnWorkerCallBack;
    private Socket mainSocket;
    //private Socket[] workerSocket = new Socket[MAX_CLIENTS];
    private System.Collections.ArrayList connectionPackets =
            ArrayList.Synchronized(new System.Collections.ArrayList());

    private int clientCount = 0;

    public Form1 form = null;

    public telnetClass(Form1 form)
    {
        this.form = form;
        // Display the local IP address on the GUI
    }

    public static List<string> getLocalIPAddressList()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        List<string> addresses = new List<string>();
        addresses.Add("127.0.0.1");
        foreach (var ip in host.AddressList)
        {
                addresses.Add(ip.ToString());
        }
        return addresses;
    }

    public void startListening()
    {
        try
        {
            string portStr = form.getPortText();
            string ipStr = form.getIpAddressStringFromCombo();
            // Check the port value
            if (portStr == "") {
                form.setText("Please enter a Port Number \n");
                return;
            }
            if (ipStr == "")
            {
                form.setText("No ip address specified \n");
                return;
            }

       
            int port = System.Convert.ToInt32(portStr);
            mainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipLocal = new IPEndPoint (IPAddress.Parse(ipStr), port);
            mainSocket.Bind(ipLocal);
            mainSocket.Listen (0);
            mainSocket.BeginAccept(new AsyncCallback (acceptConnection), null);
            form.setText("Waiting for connnection... \n");
            form.updateTelnetBtns(true);
        }
        catch (SocketException se)
        {
            form.setText(se.Message + '\n');
        }

    }

    public void acceptConnection(IAsyncResult result)
    {
        try
        {
           Socket  workerSocket = mainSocket.EndAccept(result);
           

           SocketPacket theSocPkt = new SocketPacket (workerSocket, clientCount);
           connectionPackets.Add(theSocPkt);
           waitForData(theSocPkt);
           dataSend(form.getInformationString(), theSocPkt);
           String str = String.Format("Client  {0} connected {1} \n", clientCount, workerSocket.RemoteEndPoint.ToString());
           form.setText(str);

            //startSerialBeginTimeout(2000, clientCount);
       

            
            // Since the main Socket is now free, it can go back and wait for
            // other clients who are attempting to connect
            Interlocked.Increment(ref clientCount);
      
            mainSocket.BeginAccept(new AsyncCallback(acceptConnection), null);

        }
        catch (ObjectDisposedException)
        {
            System.Diagnostics.Debugger.Log(0, "1", "\n OnClientConnection: Socket has been closed\n");
        }
        catch (SocketException se)
        {
            form.setText(se.Message + '\n');
        }

    }

    public class SocketPacket
    {
        public SocketPacket(System.Net.Sockets.Socket socket, int clientNumber)
        {
            currentSocket = socket;
            this.clientNumber = clientNumber;
        }
        public System.Net.Sockets.Socket currentSocket;
        public int clientNumber;
        public int zeroCount;
        public int skipBytes;
        // Buffer to store the data sent by the client
        public byte[] dataBuffer = new byte[1];
    }

    public bool socketConnected(Socket s)
    {
        bool part1 = s.Poll(1000, SelectMode.SelectRead);
        bool part2 = (s.Available == 0);
        if (part1 && part2)
            return false;
        else
            return true;
    }

    public void waitForData(SocketPacket theSocPkt)
    {
        try
        {
            Socket soc = theSocPkt.currentSocket;
            if(socketConnected(soc) == true)
            {

                pfnWorkerCallBack = new AsyncCallback (dataReceived);


                // Start receiving any data written by the connected client
                // asynchronously
                soc .BeginReceive (theSocPkt.dataBuffer, 0,
                                   theSocPkt.dataBuffer.Length,
                                   SocketFlags.None,
                                   pfnWorkerCallBack,
                                   theSocPkt);
            }
            else
            {
                closeSocket(theSocPkt);
            }
        }
        catch (SocketException se)
        {
            form.setText(se.Message +'\n');
        }

    }

    public void dataReceived(IAsyncResult asyn)
    {
        try
        {
                SocketPacket socketData = (SocketPacket)asyn.AsyncState;

                int iRx = 0;

                iRx = socketData.currentSocket.EndReceive(asyn);
                foreach(byte receivedChar in socketData.dataBuffer)
                    {
                        if(socketData.skipBytes > 0)
                        {
                            socketData.skipBytes--;
                        }
                        else if (receivedChar == 0xff)
                        {
                            form.setText("Received telnet command \n");
                            socketData.skipBytes = 2;
                        }
                        else
                        {
                            form.telnetReceivedChar(receivedChar);
                        }
                    }
                waitForData(socketData);
        }
        catch (ObjectDisposedException )
        {
            System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
        }
        catch (SocketException se)
        {
            form.setText(se.Message +'\n');
        }
    }

    public void closeSocket(SocketPacket connectionPacket)
    {
        Socket workerSocket = connectionPacket.currentSocket;
        if (workerSocket != null)
        {
            workerSocket.Close();
            workerSocket = null;
        }
        String str = String.Format("Connection {0} closed \n",connectionPacket.clientNumber );
        form.setText(str);
        connectionPackets.Remove(connectionPacket);
    }

    public void closeSockets()
    {
        for (int i = connectionPackets.Count - 1; i >= 0; i--)
        {
                closeSocket((SocketPacket)connectionPackets[i]);
        }
        if (mainSocket != null)
        {
            mainSocket.Close();
        }
    }
    public void dataSend(string data, SocketPacket socPac)
    {
        try
        {
            Socket workerSocket = socPac.currentSocket;
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(data);
            if (workerSocket != null && workerSocket.Connected) {
                workerSocket.Send(byData);
            }
        }
        catch (SocketException se)
        {
            form.setText(se.Message +'\n' );
        }
    }

    public void dataSendBroadcast(string data)
    {
        try
        {
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(data);
            Socket workerSocket = null;
            foreach (SocketPacket connectionPacket in connectionPackets)
            {
                workerSocket = connectionPacket.currentSocket; 
                if (workerSocket != null && workerSocket.Connected)
                {
                    workerSocket.Send(byData);
                }
            }
        }
        catch (SocketException se)
        {
            form.setText(se.Message + '\n');
        }
    }


}
}
