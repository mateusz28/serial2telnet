﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Data;
using System.Management;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SerialTelnet
{
    class serialClass
    {
        
        SerialPort ComPort = null;
        internal delegate void port_DataReceivedCallback(object sender, SerialDataReceivedEventArgs e);
        Form1 form = null;

        string InputData = String.Empty;
        public serialClass(Form1 form)
        {
           this.ComPort = new SerialPort();
           ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived);
           this.form = form;
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            InputData = ComPort.ReadExisting();
            form.serialReceived(InputData);
        }

        public void port_DataSend(byte[] data, int length)
        {
            try
            {
                ComPort.Write(data, 0 ,length);
            }
            catch (InvalidOperationException io)
            {
                form.setText(io.Message + '\n');
            }
        }

        public void port_DataSend(string data)
        {
            try
            {
                ComPort.Write(data);
            }
            catch (InvalidOperationException io)
            {
                form.setText(io.Message + '\n');
            }
        }

        public void port_Initialize(string portName, int baudRate, int dataBits = 8, string stopBits = "1", string handshake = "None", string parity = "None")
        {
            try
            {
                ComPort.PortName = portName;
                ComPort.BaudRate = baudRate;
                ComPort.DataBits = dataBits;
                ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), stopBits);
                ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), handshake);
                ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), parity);
            }
            catch
            {
                form.setText("Serial port error\n");
                throw new System.ArgumentException("Wrong parameters", "parameters");
            }
            try
            {
                ComPort.Open();
                
            }
            catch (UnauthorizedAccessException ex)
            {
                form.setText(ex.Message + '\n');
                throw new System.ArgumentException("Port cannot be opened");
            }
            catch
            {
                form.setText("Port cannot be opened\n");
                throw new System.ArgumentException("Port cannot be opened");
            }
            form.setText("Serial port opened\n");
        }

        public void port_Close()
        {
          ComPort.Close();
          form.setText("Serial port closed\n");
        }

        public static List<string> get_Portnames()
        {
            List<string> tList = new List<string>();
            try
            {
                //TODO: Adjust where clause to find your device(s)
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity WHERE Caption LIKE '%(COM%'");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    tList.Add(queryObj["Caption"].ToString());
                }
                return tList;
            }
            catch (ManagementException e)
            {
                MessageBox.Show("An error occurred while querying for WMI data: " + e.Message);
                return null;
            }
            
        }

        public string getShortPortname(string input)
        {
            Regex regex = new Regex(@"\bCOM\d+\b");

   

            try
            {
                Match match = Regex.Match(input, @"\bCOM\d+\b");
                if (match.Success)
                {
                    form.setText(match.Value + ' ');
                    return match.Value;
                }
                else
                {
                    form.setText("Serial port not available\n");
                    return "";
                }
            }
            catch
            {
                form.setText("Serial port not available\n");
                return "";
            }
        }
    }
}
