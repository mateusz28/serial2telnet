﻿namespace SerialTelnet
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.getPorts = new System.Windows.Forms.Button();
            this.portSelectCombo = new System.Windows.Forms.ComboBox();
            this.baudRateCombo = new System.Windows.Forms.ComboBox();
            this.parametersLabel = new System.Windows.Forms.Label();
            this.baudRateLabel = new System.Windows.Forms.Label();
            this.portOpen = new System.Windows.Forms.Button();
            this.comPreview = new System.Windows.Forms.RichTextBox();
            this.ipAddresComboLabel = new System.Windows.Forms.Label();
            this.ipAddresCombo = new System.Windows.Forms.ComboBox();
            this.portLabel = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.telnetOpenBtn = new System.Windows.Forms.Button();
            this.telnetCloseBtn = new System.Windows.Forms.Button();
            this.closeSerialBtn = new System.Windows.Forms.Button();
            this.comPortTitle = new System.Windows.Forms.Label();
            this.telnetSettingsLab = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.Label();
            this.stopBitsLabel = new System.Windows.Forms.Label();
            this.stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.parityLabel = new System.Windows.Forms.Label();
            this.parityComboBox = new System.Windows.Forms.ComboBox();
            this.handshakeLabel = new System.Windows.Forms.Label();
            this.handshakeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataBitsComboBox = new System.Windows.Forms.ComboBox();
            this.copyright = new System.Windows.Forms.Label();
            this.notifyIcon2 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // getPorts
            // 
            this.getPorts.Location = new System.Drawing.Point(10, 31);
            this.getPorts.Name = "getPorts";
            this.getPorts.Size = new System.Drawing.Size(75, 23);
            this.getPorts.TabIndex = 0;
            this.getPorts.Text = "Refresh";
            this.getPorts.UseVisualStyleBackColor = true;
            this.getPorts.Click += new System.EventHandler(this.getPorts_click);
            // 
            // portSelectCombo
            // 
            this.portSelectCombo.FormattingEnabled = true;
            this.portSelectCombo.Location = new System.Drawing.Point(10, 60);
            this.portSelectCombo.Name = "portSelectCombo";
            this.portSelectCombo.Size = new System.Drawing.Size(318, 21);
            this.portSelectCombo.TabIndex = 2;
            // 
            // baudRateCombo
            // 
            this.baudRateCombo.FormattingEnabled = true;
            this.baudRateCombo.Location = new System.Drawing.Point(76, 104);
            this.baudRateCombo.Name = "baudRateCombo";
            this.baudRateCombo.Size = new System.Drawing.Size(121, 21);
            this.baudRateCombo.TabIndex = 3;
            // 
            // parametersLabel
            // 
            this.parametersLabel.AutoSize = true;
            this.parametersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.parametersLabel.Location = new System.Drawing.Point(10, 84);
            this.parametersLabel.Name = "parametersLabel";
            this.parametersLabel.Size = new System.Drawing.Size(81, 17);
            this.parametersLabel.TabIndex = 4;
            this.parametersLabel.Text = "Parameters";
            // 
            // baudRateLabel
            // 
            this.baudRateLabel.AutoSize = true;
            this.baudRateLabel.Location = new System.Drawing.Point(13, 108);
            this.baudRateLabel.Name = "baudRateLabel";
            this.baudRateLabel.Size = new System.Drawing.Size(58, 13);
            this.baudRateLabel.TabIndex = 5;
            this.baudRateLabel.Text = "Baud Rate";
            // 
            // portOpen
            // 
            this.portOpen.Location = new System.Drawing.Point(91, 31);
            this.portOpen.Name = "portOpen";
            this.portOpen.Size = new System.Drawing.Size(75, 23);
            this.portOpen.TabIndex = 6;
            this.portOpen.Text = "Open";
            this.portOpen.UseVisualStyleBackColor = true;
            this.portOpen.Click += new System.EventHandler(this.portOpen_Click);
            // 
            // comPreview
            // 
            this.comPreview.Location = new System.Drawing.Point(10, 343);
            this.comPreview.Name = "comPreview";
            this.comPreview.Size = new System.Drawing.Size(311, 127);
            this.comPreview.TabIndex = 7;
            this.comPreview.Text = "";
            this.comPreview.TextChanged += new System.EventHandler(this.comPreview_TextChanged);
            // 
            // ipAddresComboLabel
            // 
            this.ipAddresComboLabel.AutoSize = true;
            this.ipAddresComboLabel.Location = new System.Drawing.Point(13, 301);
            this.ipAddresComboLabel.Name = "ipAddresComboLabel";
            this.ipAddresComboLabel.Size = new System.Drawing.Size(58, 13);
            this.ipAddresComboLabel.TabIndex = 10;
            this.ipAddresComboLabel.Text = "IP Address";
            // 
            // ipAddresCombo
            // 
            this.ipAddresCombo.FormattingEnabled = true;
            this.ipAddresCombo.Location = new System.Drawing.Point(76, 296);
            this.ipAddresCombo.Name = "ipAddresCombo";
            this.ipAddresCombo.Size = new System.Drawing.Size(156, 21);
            this.ipAddresCombo.TabIndex = 9;
            this.ipAddresCombo.SelectedIndexChanged += new System.EventHandler(this.ipAddresCombo_SelectedIndexChanged);
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(235, 299);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(26, 13);
            this.portLabel.TabIndex = 11;
            this.portLabel.Text = "Port";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(265, 296);
            this.portTextBox.MaxLength = 5;
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(59, 20);
            this.portTextBox.TabIndex = 12;
            this.portTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.portTextBox_KeyPress);
            // 
            // telnetOpenBtn
            // 
            this.telnetOpenBtn.Location = new System.Drawing.Point(10, 267);
            this.telnetOpenBtn.Name = "telnetOpenBtn";
            this.telnetOpenBtn.Size = new System.Drawing.Size(75, 23);
            this.telnetOpenBtn.TabIndex = 13;
            this.telnetOpenBtn.Text = "Open";
            this.telnetOpenBtn.UseVisualStyleBackColor = true;
            this.telnetOpenBtn.Click += new System.EventHandler(this.telnetOpenBtn_Click);
            // 
            // telnetCloseBtn
            // 
            this.telnetCloseBtn.Location = new System.Drawing.Point(91, 267);
            this.telnetCloseBtn.Name = "telnetCloseBtn";
            this.telnetCloseBtn.Size = new System.Drawing.Size(75, 23);
            this.telnetCloseBtn.TabIndex = 14;
            this.telnetCloseBtn.Text = "Close";
            this.telnetCloseBtn.UseVisualStyleBackColor = true;
            this.telnetCloseBtn.Click += new System.EventHandler(this.telnetCloseBtn_Click);
            // 
            // closeSerialBtn
            // 
            this.closeSerialBtn.Location = new System.Drawing.Point(172, 31);
            this.closeSerialBtn.Name = "closeSerialBtn";
            this.closeSerialBtn.Size = new System.Drawing.Size(75, 23);
            this.closeSerialBtn.TabIndex = 15;
            this.closeSerialBtn.Text = "Close";
            this.closeSerialBtn.UseVisualStyleBackColor = true;
            this.closeSerialBtn.Click += new System.EventHandler(this.closeSerialBtn_Click);
            // 
            // comPortTitle
            // 
            this.comPortTitle.AutoSize = true;
            this.comPortTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comPortTitle.Location = new System.Drawing.Point(6, 8);
            this.comPortTitle.Name = "comPortTitle";
            this.comPortTitle.Size = new System.Drawing.Size(145, 20);
            this.comPortTitle.TabIndex = 16;
            this.comPortTitle.Text = "Serial Port Settings";
            // 
            // telnetSettingsLab
            // 
            this.telnetSettingsLab.AutoSize = true;
            this.telnetSettingsLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.telnetSettingsLab.Location = new System.Drawing.Point(6, 240);
            this.telnetSettingsLab.Name = "telnetSettingsLab";
            this.telnetSettingsLab.Size = new System.Drawing.Size(116, 20);
            this.telnetSettingsLab.TabIndex = 17;
            this.telnetSettingsLab.Text = "Telnet Settings";
            // 
            // log
            // 
            this.log.AutoSize = true;
            this.log.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.log.Location = new System.Drawing.Point(6, 320);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(36, 20);
            this.log.TabIndex = 18;
            this.log.Text = "Log";
            // 
            // stopBitsLabel
            // 
            this.stopBitsLabel.AutoSize = true;
            this.stopBitsLabel.Location = new System.Drawing.Point(13, 163);
            this.stopBitsLabel.Name = "stopBitsLabel";
            this.stopBitsLabel.Size = new System.Drawing.Size(49, 13);
            this.stopBitsLabel.TabIndex = 20;
            this.stopBitsLabel.Text = "Stop Bits";
            // 
            // stopBitsCombo
            // 
            this.stopBitsCombo.FormattingEnabled = true;
            this.stopBitsCombo.Location = new System.Drawing.Point(76, 159);
            this.stopBitsCombo.Name = "stopBitsCombo";
            this.stopBitsCombo.Size = new System.Drawing.Size(121, 21);
            this.stopBitsCombo.TabIndex = 19;
            // 
            // parityLabel
            // 
            this.parityLabel.AutoSize = true;
            this.parityLabel.Location = new System.Drawing.Point(13, 190);
            this.parityLabel.Name = "parityLabel";
            this.parityLabel.Size = new System.Drawing.Size(33, 13);
            this.parityLabel.TabIndex = 22;
            this.parityLabel.Text = "Parity";
            // 
            // parityComboBox
            // 
            this.parityComboBox.FormattingEnabled = true;
            this.parityComboBox.Location = new System.Drawing.Point(76, 187);
            this.parityComboBox.Name = "parityComboBox";
            this.parityComboBox.Size = new System.Drawing.Size(121, 21);
            this.parityComboBox.TabIndex = 21;
            // 
            // handshakeLabel
            // 
            this.handshakeLabel.AutoSize = true;
            this.handshakeLabel.Location = new System.Drawing.Point(13, 218);
            this.handshakeLabel.Name = "handshakeLabel";
            this.handshakeLabel.Size = new System.Drawing.Size(62, 13);
            this.handshakeLabel.TabIndex = 24;
            this.handshakeLabel.Text = "Handshake";
            // 
            // handshakeComboBox
            // 
            this.handshakeComboBox.FormattingEnabled = true;
            this.handshakeComboBox.Location = new System.Drawing.Point(76, 214);
            this.handshakeComboBox.Name = "handshakeComboBox";
            this.handshakeComboBox.Size = new System.Drawing.Size(121, 21);
            this.handshakeComboBox.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Data Bits";
            // 
            // dataBitsComboBox
            // 
            this.dataBitsComboBox.FormattingEnabled = true;
            this.dataBitsComboBox.Location = new System.Drawing.Point(76, 131);
            this.dataBitsComboBox.Name = "dataBitsComboBox";
            this.dataBitsComboBox.Size = new System.Drawing.Size(121, 21);
            this.dataBitsComboBox.TabIndex = 25;
            // 
            // copyright
            // 
            this.copyright.AutoSize = true;
            this.copyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.copyright.Location = new System.Drawing.Point(205, 476);
            this.copyright.Name = "copyright";
            this.copyright.Size = new System.Drawing.Size(116, 9);
            this.copyright.TabIndex = 27;
            this.copyright.Text = "@Copyright Mateusz Orzoł 2016";
            // 
            // notifyIcon2
            // 
            this.notifyIcon2.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon2.Icon")));
            this.notifyIcon2.Text = "notifyIcon2";
            this.notifyIcon2.Visible = true;
            this.notifyIcon2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon2_MouseDoubleClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 487);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataBitsComboBox);
            this.Controls.Add(this.handshakeLabel);
            this.Controls.Add(this.handshakeComboBox);
            this.Controls.Add(this.parityLabel);
            this.Controls.Add(this.parityComboBox);
            this.Controls.Add(this.stopBitsLabel);
            this.Controls.Add(this.stopBitsCombo);
            this.Controls.Add(this.log);
            this.Controls.Add(this.telnetSettingsLab);
            this.Controls.Add(this.comPortTitle);
            this.Controls.Add(this.closeSerialBtn);
            this.Controls.Add(this.telnetCloseBtn);
            this.Controls.Add(this.telnetOpenBtn);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.ipAddresComboLabel);
            this.Controls.Add(this.ipAddresCombo);
            this.Controls.Add(this.comPreview);
            this.Controls.Add(this.portOpen);
            this.Controls.Add(this.baudRateLabel);
            this.Controls.Add(this.parametersLabel);
            this.Controls.Add(this.baudRateCombo);
            this.Controls.Add(this.portSelectCombo);
            this.Controls.Add(this.getPorts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Serial2Telnet";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button getPorts;
        private System.Windows.Forms.ComboBox portSelectCombo;
        private System.Windows.Forms.ComboBox baudRateCombo;
        private System.Windows.Forms.Label parametersLabel;
        private System.Windows.Forms.Label baudRateLabel;
        private System.Windows.Forms.Button portOpen;
        private System.Windows.Forms.RichTextBox comPreview;
        private System.Windows.Forms.Label ipAddresComboLabel;
        private System.Windows.Forms.ComboBox ipAddresCombo;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Button telnetOpenBtn;
        private System.Windows.Forms.Button telnetCloseBtn;
        private System.Windows.Forms.Button closeSerialBtn;
        private System.Windows.Forms.Label comPortTitle;
        private System.Windows.Forms.Label telnetSettingsLab;
        private System.Windows.Forms.Label log;
        private System.Windows.Forms.Label stopBitsLabel;
        private System.Windows.Forms.ComboBox stopBitsCombo;
        private System.Windows.Forms.Label parityLabel;
        private System.Windows.Forms.ComboBox parityComboBox;
        private System.Windows.Forms.Label handshakeLabel;
        private System.Windows.Forms.ComboBox handshakeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox dataBitsComboBox;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.NotifyIcon notifyIcon2;
    }
}

