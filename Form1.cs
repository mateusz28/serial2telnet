﻿using System;
using System.Collections.Generic;
using System.Management;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace SerialTelnet
{
    public partial class Form1 : Form
    {
        serialClass serial = null;
        telnetClass telnet = null;
        public string informationString = "";
        delegate void SetTextCallback(string text);
        
  


        public Form1()
        {
       
            InitializeComponent();
            this.serial = new serialClass(this);
            this.telnet = new telnetClass(this);
            portTextBox.Text = "51234";
           
            fillBaudRateCombo();
            fillParityCombo();
            fillHandshakeCombo();
            fillStopBitsCombo();
            fillDataBitsCombo();
            refreshIpAddressList();
            updateTelnetBtns(false);
            updateSerialBtns(false);
            refreshPortsList();
        }

        private void getPorts_click(object sender, EventArgs e)
        {
            refreshPortsList();
        }

        private void refreshPortsList()
        {
       
            using (var searcher = new ManagementObjectSearcher("root\\CIMV2",
    "SELECT * FROM Win32_PnPEntity WHERE ClassGuid=\"{4d36e978-e325-11ce-bfc1-08002be10318}\""))
            {
                portSelectCombo.Items.Clear();
                var tList = serialClass.get_Portnames();

                if (!tList.Any())
                {
                    portSelectCombo.Text = "No serial devices detected";
                    portOpen.Enabled = false;
                }
                else
                {
                    foreach (string s in tList)
                    {
                        portSelectCombo.Items.Add(s);
                    }
                    portOpen.Enabled = true;
                    portSelectCombo.Text = tList[0];
                }

            }
        }


        private void refreshIpAddressList()
        {
            var tList = telnetClass.getLocalIPAddressList();

            foreach (string s in tList)
            {
                ipAddresCombo.Items.Add(s);
            }
            ipAddresCombo.Text = tList[0];
        }

        public string getIpAddressStringFromCombo()
        {
            return ipAddresCombo.Text;
        }

        private void fillBaudRateCombo()
        {
            baudRateCombo.Items.Add(9600);
            baudRateCombo.Items.Add(19200);
            baudRateCombo.Items.Add(38400);
            baudRateCombo.Items.Add(57600);
            baudRateCombo.Items.Add(115200);
            baudRateCombo.Items.ToString();
            baudRateCombo.Text = baudRateCombo.Items[4].ToString();
        }

        private void fillParityCombo()
        {
            parityComboBox.Items.Add("None");
            parityComboBox.Items.Add("Even");
            parityComboBox.Items.Add("Odd");
            parityComboBox.Items.ToString();
            parityComboBox.Text = parityComboBox.Items[0].ToString();
        }

        private void fillHandshakeCombo()
        {
            handshakeComboBox.Items.Add("None");
            handshakeComboBox.Items.Add("XOnXOff");
            handshakeComboBox.Items.ToString();
            handshakeComboBox.Text = handshakeComboBox.Items[0].ToString();
        }

        private void fillStopBitsCombo()
        {
            stopBitsCombo.Items.Add("One");
            stopBitsCombo.Items.Add("Two");
            stopBitsCombo.Items.ToString();
            stopBitsCombo.Text = stopBitsCombo.Items[0].ToString();
        }

        private void fillDataBitsCombo()
        {
            dataBitsComboBox.Items.Add(8);
            dataBitsComboBox.Items.Add(6);
            dataBitsComboBox.Items.Add(7);
            dataBitsComboBox.Items.ToString();
            dataBitsComboBox.Text = dataBitsComboBox.Items[0].ToString();
        }


        private void portOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serial.port_Initialize(serial.getShortPortname(portSelectCombo.Text), Convert.ToInt32(baudRateCombo.Text), Convert.ToInt32(dataBitsComboBox.Text),
                    stopBitsCombo.Text, handshakeComboBox.Text, parityComboBox.Text);
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
                updateSerialBtns(false);
                return;
            }
            catch
            {
                refreshPortsList();
                updateSerialBtns(false);
                return;
            }
            finally
            {
                
            }
            updateSerialBtns(true);

        }

           private void closeSerialBtn_Click(object sender, EventArgs e)
      {
          closeSerialPort();
      }

           public string createConnectionInfo()
           {
               String str = String.Format("----------------------------\r\n");
               str += String.Format("Welcome to serial2telnet\r\n");
               str += String.Format("Connected to {0}:{1} \r\n", ipAddresCombo.Text, portTextBox.Text);
               str += String.Format("Serial device: {0} \r\n", portSelectCombo.Text);
               str += String.Format("Baud rate: {0} \r\n", baudRateCombo.Text);
               str += String.Format("Data bits: {0} \r\n", dataBitsComboBox.Text);
               str += String.Format("Parity: {0} \r\n", parityComboBox.Text);
               str += String.Format("Stop bits: {0} \r\n", stopBitsCombo.Text);
               str += String.Format("Handshake: {0} \r\n", handshakeComboBox.Text);
               str += String.Format("----------------------------\r\n");
               return str;

           }
           public string getInformationString()
           {
               return informationString;
           }

        public void closeSerialPort()
    {
        serial.port_Close();
          refreshPortsList();
          updateSerialBtns(false);
    }

        public void updateSerialBtns(bool listening)
        {
            portOpen.Enabled = !listening;
            baudRateCombo.Enabled = !listening;
            portSelectCombo.Enabled = !listening;
            handshakeComboBox.Enabled = !listening;
            parityComboBox.Enabled = !listening;
            stopBitsCombo.Enabled = !listening;
            dataBitsComboBox.Enabled = !listening;
            getPorts.Enabled = !listening;
            closeSerialBtn.Enabled = listening;

        }


        public void setText(string text)
        {
            if (this.comPreview.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.comPreview.Text += text;
                this.comPreview.ScrollToCaret();
            }
         
        }

        public string getPortText()
        {
           return portTextBox.Text;
        }

        public void telnetReceivedChar(byte data)
        {
            byte[] dataBuffer = new byte[1];
            dataBuffer[0] = data;
            serial.port_DataSend(dataBuffer, 1);
        }

        public void telnetReceived(byte [] data, int length)
        {
            serial.port_DataSend(data, length);
        }


        public void telnetReceived(string data)
        {
            serial.port_DataSend(data);
        }

        public void serialReceived(string data)
        {
            telnet.dataSendBroadcast(data);
        }

        private void testWriteButtton_Click(object sender, EventArgs e)
        {
            serial.port_DataSend("dupa");
        }

        private void ipAddresCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                    e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

        }

        private void portTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void telnetOpenBtn_Click(object sender, EventArgs e)
        {
                try
                {
                    informationString = createConnectionInfo();
                    telnet.startListening();
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show(ex.Message);
                }

        }


      public void updateTelnetBtns(bool listening)
        {
            telnetOpenBtn.Enabled = !listening;
            portTextBox.Enabled = !listening;
            ipAddresCombo.Enabled = !listening;
            telnetCloseBtn.Enabled = listening;
        }

      private void telnetCloseBtn_Click(object sender, EventArgs e)
      {
          try
          {
              telnet.closeSockets();
          }
          catch (UnauthorizedAccessException ex)
          {
              MessageBox.Show(ex.Message);
          }
          updateTelnetBtns(false);
      }

      private void comPreview_TextChanged(object sender, EventArgs e)
      {
          this.comPreview.SelectionStart = this.comPreview.Text.Length;
          this.comPreview.ScrollToCaret();
      }

 

      private void Form1_Resize(object sender, EventArgs e)
      {
          notifyIcon2.BalloonTipTitle = "Minimized to Tray";
          notifyIcon2.BalloonTipText = "Double click to show.";
          if (FormWindowState.Minimized == this.WindowState)
          {
              notifyIcon2.ShowBalloonTip(100);
              notifyIcon2.Visible = true;
              
              this.Hide();
          }

          else if (FormWindowState.Normal == this.WindowState)
          {
              notifyIcon2.Visible = false;
          }
      }

      private void notifyIcon2_MouseDoubleClick(object sender, MouseEventArgs e)
      {
          
     this.Show();
     this.WindowState = FormWindowState.Normal;
      }
   
   
    }
}

   

       